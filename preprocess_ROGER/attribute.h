#define Lg_Mot 200
#define Nb_max_values 100
#ifndef ATT_H
#define ATT_H 

class attribute {
    int num;
  char *Lst_values;
  char Nom_att[Lg_Mot];
  int statut;
  int nb_values; // nb of values
public:
  attribute(FICH_IN *Fic, int _num);
  double Lit(FICH_IN *Fic);
  int Check(const char *Mot); // check if it exists and returns its rank; otherwise, put Mot in List_values, increment nb_values and return nb_values;
  void show();
  int Signif();
  int Phase_2_Read(double v,int index);
  int get_nb_val() { return nb_values;};
  int clean_form(FILE *Fic, int wr);
  int clean_form_class(FILE *Fic, int wr);
  int Genere_New_Learn(FILE *Fic, double v);
};

typedef attribute *pattribute;

TYPE_EXTERN pattribute *Lst_Attribute;

TYPE_EXTERN double *memoire; // boosting like
TYPE_EXTERN int *etiquette; // boosting like
TYPE_EXTERN int nb_ex;
TYPE_EXTERN int nb_ex_test;
TYPE_EXTERN int nb_att;
TYPE_EXTERN char *Nom_DATA;
TYPE_EXTERN double* donnees; // les exemples
TYPE_EXTERN int* classe; // classe de chaque exemple, 1, 3, 5
TYPE_EXTERN int* predit; // predit[i] = nombre d'individus malades dont le score 
// est superieur au score de l'individu i.
TYPE_EXTERN double  *pile; // la ou on range le score pour les exemples, en cours de
  // calcul
TYPE_EXTERN double pile_min, pile_max;
TYPE_EXTERN int* ordre_index; // pour classer

TYPE_EXTERN int nb_true_att; // string attributes are turned into one hot encoding
TYPE_EXTERN int nb_init_att; // in the data

TYPE_EXTERN int nb_ok;
TYPE_EXTERN int nb_non_ok;

TYPE_EXTERN int IndexClass;

#endif

