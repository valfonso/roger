/* ----------------------------------------------------------------------
 * Where........: CMAP - Polytechnique 
 * File.........: erreur_msg.h
 * Author.......: Marc Schoenauer
 * Created......: Tue Mar 21 13:13:14 1995
 * Description..: Messages d'erreurs
 * 
 * Ident........: $Id: erreur_msg.h,v 1.2 1995/04/19 13:43:56 lamy Exp $
 * ----------------------------------------------------------------------
 */


const char* ERREUR[] =
   {
/*  0 */ "Impossible d'ouvrir en lecture le fichier ",
/*  1 */ "Fin de fichier dans ",
/*  2 */ "Grave erreur dans add_gauss",
/*  3 */ "Probleme d'allocation memoire",
/*  4 */ "Errur fatale",
/*  5 */ "",
/*  6 */ "",
/*  7 */ "",
/*  8 */ "",
/*  9 */ "",
/* 10 */ "Trop d'individus dans le fichier ",
/* 11 */ "Chaine non reconnue : ",
/* 12 */ "Explosion de reel : "
   };
